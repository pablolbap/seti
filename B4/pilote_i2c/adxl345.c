#include <linux/init.h>
#include <linux/miscdevice.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/of.h>
#include <linux/i2c.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <linux/kfifo.h> 
#include <linux/wait.h>

#define I2C_M_RD 0x0001
#define REG_DEVID 0x00
#define REG_FIFO_CTL 0x38
#define REG_FIFO_STATUS 0x39
#define REG_INT_ENABLE 0x2E
#define REG_INT_SOURCE 0x30
#define REG_POWER_CTL 0x2D
#define REG_DATAX0 0x32
#define REG_DATAX1 0x33
#define REG_DATAY0 0x34
#define REG_DATAY1 0x35
#define REG_DATAZ0 0x36
#define REG_DATAZ1 0x37
#define REG_ACT_INACT_CTL 0x27
#define REG_BW_RATE 0x2c

#define CHECK(x)                              \
    do                                        \
    {                                         \
        int err = (x);                        \
        if (err < 0)                          \
        {                                     \
            pr_err("Runtime error: %d", err); \
            return err;                       \
        }                                     \
    } while (0)

enum error_type
{
    SUCCESS = 0
};

enum axis
{
    X = 0,
    Y = 1,
    Z = 2
};

struct fifo_element
{
    uint8_t dataX[2];
    uint8_t dataY[2];
    uint8_t dataZ[2];
};

struct adxl345_device
{
    struct miscdevice miscdev;
    enum axis current_axis;
    uint8_t current_sample[2];
    uint8_t dev_buf[2];
    wait_queue_head_t queue;
    DECLARE_KFIFO(samples_fifo, struct fifo_element, 64);
};

struct adxl345_device * get_adxl345_device_from_client(struct i2c_client *client)
{
    return (struct adxl345_device *)i2c_get_clientdata(client);
}

long adxl345_unlocked_ioctl(struct file *file, unsigned int cmd, unsigned long arg);
ssize_t adxl345_read(struct file *file, char __user *buf, size_t count, loff_t *f_pos);

struct file_operations fops =
    {
        .owner = THIS_MODULE,
        .read = adxl345_read,
        .unlocked_ioctl = adxl345_unlocked_ioctl};

static int read_reg(struct i2c_client *client, uint8_t * buf, uint8_t register_name, uint8_t len)
{
    uint8_t i = 0;
    const char addr = register_name;
    CHECK(i2c_master_send(client, &addr, 1));

    for (i = 0; i < len; i++)
    {
        CHECK(i2c_master_recv(client, &buf[i], 1));
    }

    return SUCCESS;
}

static int write_reg(struct i2c_client *client, uint8_t register_name, char data)
{
    struct adxl345_device * dev = get_adxl345_device_from_client(client);
    uint8_t * buf = dev->dev_buf;
    buf[0] = register_name;
    buf[1] = data;
    CHECK(i2c_master_send(client, buf, 2));

    return SUCCESS;
}

static int write_bit(struct i2c_client *client, uint8_t register_name, int pos, char data)
{
    uint8_t *dbuf =  get_adxl345_device_from_client(client)->dev_buf;
    CHECK(read_reg(client, dbuf, register_name, 1));
    CHECK(write_reg(client, register_name, dbuf[0] & (0xff & ((data & 0x1) << pos))));

    return SUCCESS;
}

// static int read_bit(struct i2c_client *client, uint8_t *dbuf, uint8_t register_name, int pos)
// {
//     CHECK(read_reg(client, dbuf, register_name, 1));
//     dbuf[0] = ((dbuf[0] >> pos) & 0x1);

//     return SUCCESS;
// }

static int read_sample(struct i2c_client *client, struct fifo_element * fel)
{
    CHECK(read_reg(client, fel->dataX, REG_DATAX0, 2));
    CHECK(read_reg(client, fel->dataY, REG_DATAY0, 2));
    CHECK(read_reg(client, fel->dataZ, REG_DATAZ0, 2));

    return SUCCESS;
}

static int init(struct i2c_client *client)
{
    // Stream
    char fifo_mode = 2;     
    // 20 samples stored
    char samples = 20;  
    CHECK(write_reg(client, REG_FIFO_CTL, (fifo_mode << 6) | samples));
    // enable watermark interrupt
    CHECK(write_reg(client, REG_INT_ENABLE, 0x02));
    CHECK(write_reg(client, REG_POWER_CTL, 0x08));

    return SUCCESS;
}

irqreturn_t adxl345_watermark_irq_handler(int irq, void *dev_id)
{
    struct adxl345_device *dev = (struct adxl345_device *)dev_id;
    uint8_t n_samples;
    uint16_t waitCount = 0;
    struct fifo_element fel;
    struct i2c_client *client = container_of(dev->miscdev.parent, struct i2c_client, dev);
    CHECK(read_reg(client, &n_samples, REG_FIFO_STATUS, 2));

    do 
    {
        CHECK(read_sample(client, &fel));
        while (waitCount++ < 10000);
    
    } while (kfifo_put(&dev->samples_fifo, fel) || (n_samples-- != 0));

    wake_up(&dev->queue);

    return IRQ_HANDLED;
}

ssize_t adxl345_read(struct file *file, char __user *buf, size_t count, loff_t *f_pos)
{
    struct miscdevice *mdev = file->private_data;
    struct adxl345_device *dev = container_of(mdev, struct adxl345_device, miscdev);
    uint8_t n_bytes = (count > 2) ? 2 : count;
    struct wait_queue_head queue = dev->queue;
    struct fifo_element fel;
    wait_event_interruptible(queue, kfifo_get(&dev->samples_fifo, &fel));

    switch (dev->current_axis)
    {
    case X:
        if (copy_to_user(buf, &fel.dataX, n_bytes))
            return -EFAULT;
        break;
    case Y:
        if (copy_to_user(buf, &fel.dataY, n_bytes))
            return -EFAULT;
        break;
    case Z:
        if (copy_to_user(buf, &fel.dataZ, n_bytes))
            return -EFAULT;
        break;
    default:
        break;
    }

    return n_bytes;
}

long adxl345_unlocked_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    struct miscdevice *mdev = file->private_data;
    struct adxl345_device *dev = container_of(mdev, struct adxl345_device, miscdev);
    uint32_t tmp;
    CHECK(copy_from_user(&tmp, (uint8_t *)arg, 1));

    if(tmp > Z)
        return -EINVAL;

    dev->current_axis = tmp;
    pr_info("Selected axis : %d\n", dev->current_axis);

    return SUCCESS;
}

static int adxl345_probe(struct i2c_client *client,
                         const struct i2c_device_id *id)
{
    struct adxl345_device *dev = (struct adxl345_device *)devm_kzalloc(&client->dev, sizeof(struct adxl345_device), GFP_KERNEL);
    
    if(!dev)
        return -ENOMEM;

    dev->miscdev.parent = &client->dev;
    dev->miscdev.minor = MISC_DYNAMIC_MINOR;
    dev->miscdev.name = "adxl345";
    dev->miscdev.fops = &fops;
    INIT_KFIFO(dev->samples_fifo);
    CHECK(misc_register(&dev->miscdev));
    i2c_set_clientdata(client, dev);
    init_waitqueue_head(&dev->queue);
    CHECK(init(client));
    CHECK(devm_request_threaded_irq(&client->dev, client->irq,
                                    NULL, adxl345_watermark_irq_handler, IRQF_ONESHOT,
                                    dev->miscdev.name, dev));

    pr_info("Module loaded\n");

    return SUCCESS;
}

static int adxl345_remove(struct i2c_client *client)
{
    struct adxl345_device *dev = (struct adxl345_device *)i2c_get_clientdata(client);
    misc_deregister(&dev->miscdev);
    devm_kfree(&client->dev, dev);
    devm_free_irq(&client->dev, client->irq, dev);
    // Standby mode
    CHECK(write_bit(client, REG_POWER_CTL, 3, 1));
    pr_info("Disconnected\n");

    return SUCCESS;
}

static struct i2c_device_id adxl345_idtable[] = {
    {"adxl345", 0},
    {}};

MODULE_DEVICE_TABLE(i2c, adxl345_idtable);

#ifdef CONFIG_OF
/* Si le support des device trees est disponible, la liste suivante
   permet de faire l'association à l'aide du device tree.

   Chaque entrée contient une structure de type of_device_id. Le champ
   compatible est une chaîne qui est utilisée pour faire l'association
   avec les champs compatible dans le device tree. Le champ data est
   un pointeur void* qui peut être utilisé par le pilote pour
   effectuer des traitements différents en fonction du périphérique
   physique détecté.
   */
static const struct of_device_id adxl345_of_match[] = {
    {.compatible = "vendor,adxl345",
     .data = NULL},
    {}};

MODULE_DEVICE_TABLE(of, adxl345_of_match);
#endif

static struct i2c_driver adxl345_driver =
    {
        .driver = {
            /* Le champ name doit correspondre au nom du module
               et ne doit pas contenir d'espace */
            .name = "adxl345",
            .of_match_table = of_match_ptr(adxl345_of_match),
        },

        .id_table = adxl345_idtable,
        .probe = adxl345_probe,
        .remove = adxl345_remove};

module_i2c_driver(adxl345_driver);

MODULE_AUTHOR("Pablo Pevsner <ppevsner@enst.fr>");
MODULE_DESCRIPTION("adxl345 driver");
MODULE_LICENSE("GPL");
