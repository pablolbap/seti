#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>

int16_t sample_from_buffer(uint8_t * buf)
{
    return (buf[1] << 8) | buf[0];
}

int select_axis(int fd, uint8_t ax)
{
    return ioctl(fd, _IOW('a', 'a', uint8_t *), &ax);
}

int main(int argc, char *argv[])
{
    uint8_t *buf = (uint8_t *)malloc(2 * sizeof(uint8_t));
    int fd = open("/dev/adxl345", O_APPEND);
    if (fd == -1)
    {
        printf("Error opening file");
        return 0;
    }

    for (int i = 0; i < 300; i++)
    {
        if (select_axis(fd, 0) == 0)
        {
            read(fd, buf, 2);
            printf("Read sample: %d\n", sample_from_buffer(buf));
        }
        else
        {
            printf("ioctl error\n");
        }
    }

    return 0;
}
