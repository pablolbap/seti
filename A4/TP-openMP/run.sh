#!/bin/bash

target=$1
N=$2
N_threads=$3

OMP_NUM_THREADS=$N_threads; export OMP_NUM_THREADS
make $target && ./$target $N $N_threads