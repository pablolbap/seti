#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <sys/time.h>
#define NBITER 400000000
double f(double x)
{
  return (4.0 / (1.0 + x * x));
}
/* attention il y a un bug au niveau openMP ! */
double calcul_pi_parallele(int nb_threads, double a,
                           double b, long int n)
{
  long int i;
  double end, start, xi, valeur_pi = 0.0;
  double somme = 0.0;
  double h = (b - a) / n;

  start = omp_get_wtime();
#pragma omp parallel for num_threads(nb_threads) reduction(+ \
                                                           : somme)
  for (i = 1; i < n; i++)
  {
    xi = a + h * i;
    somme += f(xi);
  }

  valeur_pi = h * (somme + (f(a) + f(b)) / 2);
  end = omp_get_wtime();
  printf(" %8.5lf\t ", end - start);
  return valeur_pi;
}

int main(int argc, char **argv)
{
  int nb;
  double estim = 0.0;
  printf("Nb . threads\tTps . \t\tEstimation de Pi \n");
  for (nb = 1; nb <= omp_get_max_threads() + 2; nb++)
  {
    printf("%d\t\t", nb);
    fflush(stdout);
    estim = calcul_pi_parallele(nb, 0.0, 1.0, NBITER);
    printf("%.12lf\n", estim);
  }
  return (EXIT_SUCCESS);
}
