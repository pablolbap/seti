#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>

int main(int argc, char *argv[])
{
  int n = atoi(argv[1]);
  // int n_threads = atoi(argv[2]);
  float *tab = (float *)malloc(n * sizeof(float));
  for (int i = 0; i < n; i++)
  {
    tab[i] = (float)rand() / ((float)RAND_MAX / 1.0f);
  }

  float sum = 0.0f;
  double start_time = omp_get_wtime();
#pragma omp parallel for reduction(+ \
                                   : sum) schedule(dynamic, 2048)
  for (int i = 0; i < n; i++)
  {
    sum += tab[i];
  }
  double run_time = omp_get_wtime() - start_time;
  printf("%f\n", run_time);
  return 0;
}
