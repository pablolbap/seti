#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

float f(float x)
{
  return 2.17f * x;
}

int main(int argc, char *argv[])
{
  int n_cores;
  int n_threads_max;
#ifdef _OPENMP
  n_cores = omp_get_num_procs();
  n_threads_max = omp_get_max_threads();
#else
  // n_cores = omp_get_num_procs();
  // n_threads_max = omp_get_max_threads();
#endif
  // printf("n_cores: %d,  n_threads_max: %d \n", n_cores, n_threads_max);

  return 0;
}