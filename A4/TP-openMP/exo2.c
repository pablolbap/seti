#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>

float f1(float x)
{
  return 2.17f;
}

float f2(float x)
{
  return 2.17f * log(x) * cos(x);
}

int main(int argc, char *argv[])
{
  int n = atoi(argv[1]);
  // int n_threads = atoi(argv[2]);
  float *tab = (float *)malloc(n * sizeof(float));
  for (int i = 0; i < n; i++)
  {
    tab[i] = (float)rand() / ((float)RAND_MAX / 1.0f);
  }

  double start_time = omp_get_wtime();
#pragma omp parallel for
  for (int i = 0; i < n; i++)
  {
    // Question 1
    tab[i] = f1(tab[i]);
    // Question 2
    // tab[i] = f2(tab[i]);
  }
  double run_time = omp_get_wtime() - start_time;
  printf("%f\n", run_time);
  return 0;
}