import subprocess
import os
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.pyplot import figure
from numpy.random import randn
import matplotlib

# N = [1000, 10e5, 10e7, 10e8]
N = [1000, 10e5, 10e6, 10e7]
# N = [10e5, 10e6]
N_THREADS = np.arange(1, 16, 2)
# N_THREADS = np.arange(1, 16, 2)
options = ""
markers = ['^', 'D', 'x']
PERF = np.zeros((len(N), len(N_THREADS)))
colors = matplotlib.cm.rainbow(np.linspace(0, 1, 16))


def plot_perf(t):
    fig = plt.figure(figsize=(20, 13), dpi=80)
    ax = fig.add_axes([0.1, 0.1, 0.6, 0.75])

    for i in range(len(N)):
        plt.plot(N_THREADS, PERF[i, :], 'o-', label=str(N[i]),
                 color=colors[i])
    plt.xticks(N_THREADS)
    plt.xlabel("Number of threads", fontsize=10)
    plt.ylabel("Temps d'exécution en s")
    plt.legend(bbox_to_anchor=(1.0, 1),
               loc='upper right', borderaxespad=0.2)
    plt.savefig("FIGURES/exo2_" + str(i) + ".png")
    plt.show()


def exo2():
    for t in ['int']:
        for i in range(len(N)):
            for j in range(len(N_THREADS)):
                # cmd = "bash -c "+"'"+"gcc " + \
                #     OPT[i] + " -DN="+str(N[j]) + \
                #     options + \
                #     " -DTYPE=" + \
                #     t +\
                #     " -DPYTHON tp1.c -o tp1; ./tp1'"
                cmd = "./run.sh exo3 " + str(N[i]) + " " + str(N_THREADS[j])
                print(cmd)
                res = float(subprocess.getoutput(cmd))
                PERF[i, j] = res
        plot_perf(t)


exo2()
