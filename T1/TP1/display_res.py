import subprocess
import os
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.pyplot import figure
from numpy.random import randn

N = np.arange(100, 1000, 50)
# N = np.arange(12, 40, 2)
# N = [64, 128, 256, 300, 512, 800, 1024, 1500, 2048, 3000]
# N = [100, 200, 300, 400, 500, 600, 700, 800, 900,
#      1000, 1500, 1800, 2000, 2400]
# N = [1000]
# OPT = ['-O1', '-O2', '-O3']
OPT = ['-O2']
# OPT = ['-O1', '-O2', '-O3']
# OPT = ['-O0', '-O1', '-O2', '-O3']
PERF = np.zeros((len(OPT), len(N)), dtype=float)
options = ""
# options = ""
markers = ['^', 'D', 'x']


def plot_perf(t):
    fig = plt.figure(figsize=(20, 20), dpi=80)
    ax = fig.add_axes([0.1, 0.1, 0.6, 0.75])

    for i in range(len(OPT)):
        plt.plot(N, PERF[i, :], 'ro-', label=str(
            OPT[i]))
        plt.xticks(N)
        plt.xlabel("Taille du vecteur", fontsize=10)
        plt.ylabel("Nombre moyen de cycles par itération")
        plt.legend(bbox_to_anchor=(1.0, 1),
                   loc='upper right', borderaxespad=0.2)

    # plt.savefig("FIGURES/ps_partie3_double"+".png")


# def partie3():
#     for t in ['int']:
#         for i in range(len(OPT)):
#             for j in range(len(N)):
#                 cmd = "bash -c "+"'"+"gcc " + \
#                     OPT[i] + " -DN="+str(N[j]) + \
#                     options + \
#                     " -DTYPE=" + \
#                     t +\
#                     " -DPYTHON tp1.c -o tp1; ./tp1'"
#                 print(cmd)
#                 res = float(subprocess.getoutput(cmd))
#                 PERF[i, j] = res
#                 os.system("bash -c 'objdump tp1 > asm/tp1" +
#                           OPT[i] + "-T"+t + "-N" + str(N[j]) + ".lss'")
#         plot_perf(t)
#         plt.show()


def partie3():
    for t in ['float']:
        for i in range(len(OPT)):
            for j in range(len(N)):
                cmd = "bash -c "+"'"+"gcc " + \
                    OPT[i] + " -DN="+str(N[j]) + \
                    options + \
                    " -DTYPE=" + \
                    t +\
                    " tp1.c -o tp1; ./tp1'"
                # print(cmd)
                # res = float(subprocess.getoutput(cmd))
                os.system(cmd)
                # PERF[i, j] = res
        #         os.system("bash -c 'objdump tp1 > asm/tp1" +
        #                   OPT[i] + "-T"+t + "-N" + str(N[j]) + ".lss'")
        # plot_perf(t)
        # plt.show()


def debit_memoire():
    res = 0
    for t in ['char', 'short', 'int', 'float', 'double']:

        cmd = "bash -c "+"'"+"gcc -O3 -DN=3000" + \
            options + \
            " -DTYPE=" + \
            t + \
            " -DPYTHON tp1.c -o tp1; ./tp1'"

        print(float(subprocess.getoutput(cmd)))
        res += float(subprocess.getoutput(cmd))
    res /= 5
    print(res)


partie3()
# debit_memoire()
