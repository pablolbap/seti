- # TP1 : Analyse de performances

  ### Caractéristiques de ma machine

  | Processeur                   | AMD Ryzen 2500U             |
  | ---------------------------- | --------------------------- |
  | Architecture                 | x86_64                      |
  | Coeurs                       | 4                           |
  | Taille du cache L1D          | 32K (8-way set associative) |
  | Taille du cache L1I          | 64K                         |
  | Taille du cache L2           | 512K                        |
  | Taille du cache L3 (partagé) | 4096K                       |
  | Politique d'écriture         | write-back                  |

  ## 2. Optimisations du compilateur

  On veut étudier l'évolution des performances de quelques fonctions de `tp1.c` pour différents niveaux d'optimisation. 

  ### Fonction `zero()`

  J'ai choisi d'étudier en premier lieu la fonction de mise à zero des éléments d'un vecteur. J'ai fait varier la taille du vecteur et me suis intéressé à la *moyenne* du nombre de cycle par itération comme mesure des performances.

  | <img src="/home/pablo/seti/T1/TP1/FIGURES/zero_int.png" alt="zero_2" style="zoom:67%;" /> |
  | :----------------------------------------------------------: |
  | *Performance de `zero()` pour différentes optimisations et type int |

  | <img src="/home/pablo/seti/T1/TP1/FIGURES/zero_double.png" alt="zero_2" style="zoom:67%;" /> |
  | :----------------------------------------------------------: |
  | *Performance de `zero()` pour différentes optimisations et type double* |

  Les deux graphes montrent que l'écart de performance le plus flagrant concerne `O0` et `O1`, et que les performances entre `O1`, `O2` et `O3` sont sensiblement les mêmes quand la taille du vecteur est grande. `O3` est un peu avantagée pour les petits vecteurs. Je propose maintenant d'expliquer ces résultat.   

  #### <u>Ecart de performance entre `O0` et `O1`.</u> 

  J'ai généré le code assembleur de `zero()` et je m'intéresse au code correspondant à la boucle intérieure de `zero()`. Les résultats sont pour N = 10 et type de donnée float.

  ```assembly
  ; Résultat avec compilation -O0
  0000000000000b27 <zero>:
  	...
       a32:	mov    -0x8(%rbp),%eax
       a35:	cltq   
       a37:	lea    0x0(,%rax,4),%rdx
       a3f:	lea    0x46ab8ba(%rip),%rax        # 46ac300 <BF>
       a46:	movl   $0x0,(%rdx,%rax,1)
       a4d:	addl   $0x1,-0x8(%rbp)
       a51:	cmpl   $0x89543f,-0x8(%rbp)
       a58:	jle    a32 <zero+0x2e>
  	...
       
  ; Résultat avec compilation -O1
  
  0000000000000913 <zero>:
  	...
       889:	movq   $0x0,(%rax)
       890:	add    $0x8,%rax
       894:	cmp    %rax,%rsi
       897:	jne    889 <zero+0x34>
  	...
  ```

  - La version`-O1` utilise le frame pointer `%rbp` pour calculer la borne de la boucle et l'indice d'itération, tandis que la version `-O1` utilise directement les registres généraux `%ax` et `%si`. Ceci permet de ne pas faire de calcul relativement au frame pointer: en effet, dans le corps de la boucle, `O0` charge la valeur de `%rbp` dans `%ax`, se sert de la valeur de `%ax` pour faire les calculs, incrémente le frame pointer, puis compare cette valeur à la borne de boucle alors que `O1` met simplement `M(%ax) ` à 0, incrémente `%ax` et compare cette valeur à la borne stockée précédemment dans `%si`, ce qui évite des instructions supplémentaires coûteuses. 
  - La version `-01` inline les fonctions `start_timer()` et `stop_timer()` dans `zero()`. La perte de temps due au non-inlining  dans `O0` est notable pour les petites tailles de vecteur (le temps d'appel et retour des marqueurs n'étant pas négligeable par rapport au temps passé dans la boucle intérieure).

  Ces trois différences correspondent aux passes  `-freorder-blocks` et `-fomit-frame-pointer`  de gcc, présentes dans `-O1` et pas dans `-O0`. <u>Elles expliquent aussi les variations de performance entre `O0` et `O1` pour toutes les fonctions que j'étudie par la suite.</u>

  #### <u>Ecart de performance entre `O1`, `O2` et `O3`.</u>

  Le code de la boucle intérieure est le même pour `O2` que pour `O1`, pour toutes les tailles de vecteur et types de donnée. Pour cette fonction, les différences de performance entre ces deux optimisations ne sont pas significatives. 

  ```assembly
  8e0:	movl   $0x0,(%rax)
  8e6:	add    $0x4,%rax
  8ea:	cmp    %rax,%rsi
  8ed:	jne    8e0 <zero+0x40>
  ```

  *Pour la compilation `-O3`, `-floop-unroll-and-jam ` permet de dérouler la boucle sur plusieurs itérations. `-fvect-cost-model=dynamic ` permet de vectoriser les instructions qui pourraient potentiellement accélerer l'exécution du code.  Je n'analyserai pas le code produit par gcc avec cette option car il est trop compliqué, mais je noterai la présence de vectorisation (utilisation d'instructions vectorielles et des registres MM).* 

  Dans `O3`, cette boucle est remplacée par un appel de `memset` de la librairie C, ou un appel à l'instruction x86-64 `rep stos` pour les petites tailles de vecteur (pour mettre à zéro une zone mémoire, un appel à ces fonctions est donc jugé plus rapide qu'une vectorisation par gcc !).

  Les différences de performances entre les optimisations suivant les tailles de vecteurs s'expliquent donc par les différences de performances entre la boucle "naïve" de `O1` et`O2` et ces fonctions. 

  #### Comportement pour des petites tailles de vecteur

  Si une ligne de mon cache peut contenir `k` floats, alors on a un compulsory write-miss toutes les `k` exécutions de la boucle intérieure. Ces write-miss  induisent plusieurs cycles de latences qui correspondent au chargement de la ligne depuis la mémoire centrale dans le cache L1 (mon cache est write-back). Pour les petites tailles de vecteurs, cette pénalité représente une proportion important du temps d'exécution de la boucle intérieure de `zero()`. Ceci explique pourquoi on observe un plus grand nombre moyen de cycles par itération pour des petites tailles de vecteur que pour des moyennes tailles de vecteur.

  #### Comportement pour des grandes tailles de vecteur

  Quand la taille du vecteur est grande, on a des échecs (capacity miss) assez souvent qui induisent des accès à des caches de niveaux supérieurs. Le temps d'accès et le temps d'echec masquent le gain en performance de `O3` et fait qu'on peut comparer ses performances à `O1` ou `O2` .

  ### Fonction `ps()` 

  

  | <img src="/home/pablo/seti/T1/TP1/FIGURES/ps_int.png" alt="ps_double" style="zoom:67%;" /> |
  | :----------------------------------------------------------: |
  | *Performance de `ps()` pour différentes optimisations et type int* |

  | <img src="/home/pablo/seti/T1/TP1/FIGURES/ps_double.png" alt="ps_double" style="zoom:67%;" /> |
  | :----------------------------------------------------------: |
  | *Performance de `ps()` pour différentes optimisations et type double* |

  #### <u>Ecart de performance entre `O0` et `O1`.</u> 

  Là encore, les différences de performance entre ces deux optimisations s'expliquent par des réorganisations du code dans `O1` qui évitent des calculs relatifs au frame pointer et des `mov` (store) inutiles. Les calculs à effectuer étant plus complexes ici que dans `zero()`, la différence de performance est d'autant plus grande.

  #### <u>Ecart de performance entre `O1`, `O2` et `O3`.</u>

  - Les performances entre `O1` et `O2` sont similaires. Le corps de la boucle intérieur de `ps()` est en effet inchangé entre les deux optimisations;
  - <u>L'optimisation `O3` est équivalente à l'optimisation `O2`, excepté quand le type est un type entier (CF. code assembleur produit en dessous) : dans ce cas-là, une vectorisation du code est effectuée par `O3`</u>. La raison est que le type `char`, bien qu'entier, n'est pas supporté par les instructions de vectorisation SSE de x86-64. En ce qui concerne les types flottants, la vectorisation est ici impossible car l'addition de deux nombres flottants n'est pas associative donc on ne peut pas paralléliser les calculs. Autrement dit, on ne peut pas calculer indépendamment les produits `A[i]*B[i]` puis les sommer pour calculer le produit scalaire sans faire d'erreurs d'arrondis. On peut néanmoins forcer la vectorisation en faisant un compromis sur la précision obtenue avec `-fassociative-mat`.

  ```assembly
  ; Optimisation O2, int
  	 cf0:	mov    (%r8,%rax,1),%edx
       cf4:	imul   (%rdi,%rax,1),%edx
       cf8:	add    $0x4,%rax
       cfc:	add    %edx,%esi
       cfe:	cmp    $0x3d0900,%rax
       d04:	jne    cf0 <ps+0x40>
  
  ; Optimisation O3; double  
  	1f88:	movsd  (%rdi,%rax,1),%xmm0
      1f8d:	mulsd  (%rsi,%rax,1),%xmm0
      1f92:	add    $0x8,%rax
      1f96:	cmp    $0x7a1200,%rax
      1f9c:	addsd  %xmm0,%xmm1
      1fa0:	jne    1f88 <ps+0x48>    
      
  ; Optimisation O3; int
      1f65:	mov    %edx,%r10d
      1f68:	mov    %eax,%esi
      1f6a:	pxor   %xmm3,%xmm3
      1f6e:	shl    $0x20,%r10
      1f72:	mov    %esi,%esi
      1f74:	xor    %eax,%eax
      1f76:	or     %rsi,%r10
      1f79:	nopl   0x0(%rax)
      1f80:	movdqa (%r8,%rax,1),%xmm0
      1f86:	movdqa (%r8,%rax,1),%xmm1
      1f8c:	pmuludq (%rdi,%rax,1),%xmm0
      1f91:	pshufd $0x8,%xmm0,%xmm0
      1f96:	movdqa (%rdi,%rax,1),%xmm2
      1f9b:	psrlq  $0x20,%xmm1
      1fa0:	add    $0x10,%rax
      1fa4:	cmp    $0x3d0900,%rax
      1faa:	psrlq  $0x20,%xmm2
      1faf:	pmuludq %xmm2,%xmm1
      1fb3:	pshufd $0x8,%xmm1,%xmm1
      1fb8:	punpckldq %xmm1,%xmm0
      1fbc:	paddd  %xmm0,%xmm3
      1fc0:	jne    1f80 <ps+0x40>
      1fc2:	movdqa %xmm3,%xmm0
      1fc6:	psrldq $0x8,%xmm0
      1fcb:	paddd  %xmm0,%xmm3
      1fcf:	movdqa %xmm3,%xmm0
      1fd3:	psrldq $0x4,%xmm0
      1fd8:	paddd  %xmm0,%xmm3
      
  ; Optimisation O3, char
      23d0:	movzbl (%r8,%rdx,1),%eax
      23d5:	mulb   (%rdi,%rdx,1)
      23d8:	add    $0x1,%rdx
      23dc:	add    %eax,%esi
      23de:	cmp    $0xf4240,%rdx
      23e5:	jne    23d0 <ps+0x40>
      
      cf0:	movzbl (%r8,%rdx,1),%eax
      cf5:	mulb   (%rdi,%rdx,1)
      cf8:	add    $0x1,%rdx
      cfc:	add    %eax,%esi
      cfe:	cmp    $0xf4240,%rdx
      d05:	jne    cf0 <ps+0x40>
  ```

  

  ## 3. Etudes de diverses fonctions

  On compile maintenant en `O2`. 

  #### Remarques d'ordre général

  1. Pour tous les programmes et pour les petites tailles de vecteur, c'est le temps d'échec lié aux compulsory miss qui prédomine dans le calcul du nombre moyen de cycle par itération. Ceci correspond à un stade distinct dans l'évolution du CPI moyen des programmes. 

  2. A partir d'une taille de vecteur assez grande (qui varie selon le type des données), on observe une augmentation du nombre de cycle moyen par itération car à partir d'un certain nombre de lignes référencées, les caches seront remplis et on aura un échec de capacité à chaque nouvelle ligne référencée.

  Pour la fonction `zero()` par exemple, et pour le type double (voir en dessous),  on peut voir que :

  - Le temps d'échec lié aux échecs obligatoires prédomine pour N < 100 (premier point d'inflexion de la courbe du CPI moyen).
  - La "saturation" des caches est atteinte à peu près pour une taille de vecteur égale à 600*600 =360000 double (deuxième point d'inflexion de la courbe du CPI moyen) à peu près.

  ### 3.1 Mise à zéro d'un vecteur

On s'intéresse aux performances de `zero()` pour différentes tailles de vecteurs.

| <img src="/home/pablo/seti/T1/TP1/FIGURES/3_zero_double.png" alt="zero_2" style="zoom:67%;" /> |
| :----------------------------------------------------------: |
| *Performance de `zero()` pour différentes tailles de vecteur (type double)* |

| <img src="/home/pablo/seti/T1/TP1/FIGURES/debit_float.png" alt="zero_2" style="zoom:67%;" /> |
| :----------------------------------------------------------: |
| *Performance de `zero()` pour différentes tailles de vecteur (type float)* |

| <img src="/home/pablo/seti/T1/TP1/FIGURES/debit_int.png" alt="zero_2" style="zoom:67%;" /> |
| :----------------------------------------------------------: |
| *Performance de `zero()` pour différentes tailles de vecteur (type int)* |

| <img src="/home/pablo/seti/T1/TP1/FIGURES/debit_shortshort.png" alt="zero_2" style="zoom:67%;" /> |
| :----------------------------------------------------------: |
| *Performance de `zero()` pour différentes tailles de vecteur (type short)* |

| <img src="/home/pablo/seti/T1/TP1/FIGURES/debit_char.png" alt="zero_2" style="zoom:67%;" /> |
| :----------------------------------------------------------: |
| *Performance de `zero()` pour différentes tailles de vecteur (type char)* |





#### Calcul du débit d'écriture mémoire.

- On note $CPI_{type_sat}$ le nombre moyen de cycle par itération, pour le programme compilé avec `-DTYPE=type`, dans un cas où la taille de vecteur est assez grosse pour qu'on parvienne à un état où a que des erreurs de capacité tôt dans l'exécution d'u programme. Cela correspond au deuxième plateau dans les courbes de vitesse d'exécution présentées précemment.

 En notant $CPO_M$ le nombre moyen de cycle pour écrire un octet en mémoire principale et $S_{type}$ la taille en octet du type de donnée considéré, on a $CPI_{sat}  \approx  + CPO_{M} * S_{type}$ (car alors le temps d'accès mémoire prédomine dans le calcul du temps moyen par itération) et donc  $ CPO_{M} \approx CPI_{sat}/S_{type} $.

En moyennant sur les différents types de données $\bar{CPO}_M = (3.25/8 + 1.4/4 + 1.5/4 + 0.7/2 + 0.4/1)/5$  **= 0.40 Cycle/Octet.**

Une vitesse typique de mon processeur est **1309.838MHz** (en cycle par seconde). Dans ce cas, le débit d'écriture mémoire est à peu près égal à $(1309.838/0.40)*10^6 o/s = 3274*10^6 o/s = 3274*10^6 / 2^{30} Go/s \approx 3 Go/s$.

### 3.2 Copie de matrices



| <img src="/home/pablo/seti/T1/TP1/FIGURES/copy_ij.png" alt="ps_double" style="zoom:67%;" /> |
| :----------------------------------------------------------: |
| *Performance de `copy_ij()` pour différentes tailles de vecteur* |

| <img src="/home/pablo/seti/T1/TP1/FIGURES/copy_ji.png" alt="ps_double" style="zoom:67%;" /> |
| :----------------------------------------------------------: |
| *Performance de `copy_ji()` pour différentes tailles de vecteur* |



On remarque que, approximativement les performances de `copy_ji()` se dégradent quand la taille du vecteur augmente (jusqu'à un certain point).   `copy_ij()` a la même performance que `copy_ji()` pour les petites tailles de vecteur, puis a de bien meilleures performances quand la taille du vecteur augmente. 

On note $S$ la taille d'une ligne du cache $L1$ et $N$ la taille d'une ligne du vecteur, en octets. Je distingue deux situations :

1. Quand $N*N \leq S$, alors les performances des deux fonctions sont les mêmes car on acquiert tout le contenu des deux vecteurs AF et YF dans le cache après les deux premiers échecs obligatoires (write miss pour AF et read miss pour YF). <u>Le taux d'échec est de $2/(N*N)$ pour les deux fonctions.</u>
2. Quand $N \leq S \leq N*N$, il y a deux erreurs toutes les $\lceil{S/N}\rceil$ itérations pour `copy_ji` et une erreur toutes les $S$ itérations pour `copy_ij()`. 
3. Sinon, quand $S \leq N$ il y a deux erreurs toutes les $S$ itérations de la boucle sur i, c'est à dire un taux d'erreur de $2/S$ pour `copy_ij()` et un taux d'erreur de $2$ pour `copy_ji()` car on a deux erreurs toutes les itérations de la boucle sur i comme on change de ligne dans le cache à chaque changement de ligne dans la matrice.

Sur les graphiques, on voit que: 

- Les performances des deux fonctions sont comparables pour des petites tailles de vecteur, ce qui correspond bien à la situation 1.
- A partir d'un certain point, le nombre de cycle par itération de `copy_ji()` est proportionnel à N tandis que le nombre de cycle par itération de `copy_ij()` est constant, ce qui correspond bien à la situation 2.
- Pour N > 600 les performances se dégradent pour les deux fonctions. Les caches étant "saturés" (beaucoup d'erreurs de capacité et de conflit à l'écriture), à chaque fois qu'une ligne correspondant à un `A[i][j]` est sortie du cache, on doit la réécrire en mémoire ce qui entraîne un temps d'écriture important dans le calcul du CPI moyen.

Pour `copy_ji()`, on a également une perte en performance dès que les vecteurs utilisés sont alignés avec la taille du cache. Ceci est dû à des erreurs de conflits car puisque :

1.  Tous les éléments de AF et YF mappent sur les mêmes lignes du cache (AF et YF sont alignés avec la taille du cache).
2.  On référence toujours des nouvelles lignes de `YF`.
3.  Donc arrive à un moment de l'exécution du programme où on a référencé plus de fois la même ligne que l'associativité du cache, donc il y a des erreurs de conflits. 

Cela n'arrive pas avec `copy_ij()` car l'associativité de mes caches est supérieure à 2 donc, comme on référence tout le temps des lignse différentes, les seuls miss sont des capacity miss. 

### 3.3 Addition de matrices

| <img src="/home/pablo/seti/T1/TP1/FIGURES/add_ij.png" alt="ps_double" style="zoom:67%;" /> |
| :----------------------------------------------------------: |
| *Performance de `add_ij()` pour différentes tailles de vecteur* |

| <img src="/home/pablo/seti/T1/TP1/FIGURES/add_ji.png" alt="ps_double" style="zoom:67%;" /> |
| :----------------------------------------------------------: |
| *Performance de `add_ji()` pour différentes tailles de vecteur* |

Les mêmes remarques que pour la question précédentes sont valables. En effet, mis à part le fait que l'addition des deux valeurs (+ la lecture supplémentaire de `AF[i][j]` ) induit une petite augmentation du nombre de cycle par itération, les deux ensembles de fonctions sont équivalents. 

### 3.4 Produit scalaire de deux vecteurs



| <img src="/home/pablo/seti/T1/TP1/FIGURES/ps_partie3_double.png" alt="ps_double" style="zoom:67%;" /> |
| :----------------------------------------------------------: |
| *Performance de `ps()` pour différentes tailles de vecteur, type double* |

| <img src="/home/pablo/seti/T1/TP1/FIGURES/ps_float_partie3.png" alt="ps_float" style="zoom:67%;" /> |
| :----------------------------------------------------------: |
| *Performance de `ps()` pour différentes tailles de vecteur, type float* |

Les mêmes remarques sont valables que pour `copy_ij()` et `add_ij()`, à la latence de la multiplication flottante près, vu que le layout mémoire des deux vecteurs est similaires que pour  `copy_ij()` et `add_ij()`.

## 3. Produit de matrices

### Produit de matrices ijk de flottants 32 bits

Pour N = 100 à N = 2000, on obtient ces résultats pour chaque itération.

```bash

```

### Produit de matrices ikj de flottants 32 bits

Les améliorations obtenues s'expliquent par le fait qu'on accède aux éléments des vecteurs toujours colonne par colonne dans la version ijk, ce qui réduit le nombre d'échec obligatoire.

```

```

### Produit de matrices ijk par blocs

Plus la taille du bloc est petite, plus on réduit le nombre d'échec lié à des conflits car machin...question Copie de matrice.

