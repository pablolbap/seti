.data
    N:  .word   12
    A:  .double 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0
    B:  .double 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0

.text
    main:
    daddi r3, r0, A; r3=&A
    daddi r4, r0, B; r4=&B
    ld r2,N(r0)		; r2=N
loop:	
    l.d f1,0(r3)		; f1=A[i]
    l.d f2,0(r4)		; f2=B[i]
    l.d f3,8(r3)		; f1=A[i+1]
    l.d f4,8(r4)		; f1=B[i+1]
    add.d f2,f2,f1		; f2=B[i]+A[i]
    sub.d f4,f4,f3		; f4=B[i+1]-A[i+1]
    daddi	r3,r3,16		; Incrementing index for A and B
    daddi	r4,r4,16		; 
    s.d f2,-16(r4)		  ; B[i]=f2
    s.d f4, -8(r4)		  ; B[i+1]=f4
    daddi r2,r2,-2		; N = N - 2
    bne r2,r0,loop		; 
    nop
    halt 