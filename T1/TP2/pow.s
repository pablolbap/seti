        .data
x:      .double 2.0
n:      .word 4
w:      .double 1.0

        .text

start:  jal pow         ; call subroutine
        nop
        halt

pow:    l.d f1,x(r0)
	l.d f0,w(r0)
	ld r1,n(r0)
loop:	dsra r2,r1,1		; r2 = n >> 1
	dsll r2,r2,1		; r2 = r2 << 1
	beq r1,r2,suite		; jump suite if n even
	nop
	mul.d f0,f0,f1		; w = w * x
suite:	dsrl r1,r1,1		; n = n / 2
	beq r1,r0, fin		; end if n == 0
	mul.d f1,f1,f1		; x = x**2
	beq r0,r0, loop
	nop
 fin:	jr r31			; return to caller
	nop
	   


