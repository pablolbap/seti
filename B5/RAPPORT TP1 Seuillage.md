# Rapport TP1 Seuillage	

## 2 Seuillage sous Matlab

### 2.1 Ferrari rouge

On obtient une image sur laquelle est présente uniquement la composante rouge en remplaçant les pixels dont l'intensité de rouge $n_r$ est inférieure à 0.7 :

```matlab
for i = 1:960
    for j = 1:1280
         nu = (ima_r(i,j))/(sqrt((ima_r(i,j))^2 + (ima_b(i,j))^2 + (ima_g(i,j))^2));
         # Si nu <= 0.7 (rouge prépondérant), on met à 0 toutes les composantes du pixel
        if nu > 0.7
            ima_out(i , j ,1) = ima(i , j, 1);
            ima_out(i , j ,2) = ima(i , j, 2);
            ima_out(i , j ,3) = ima(i , j, 3);
        else
            ima_out(i , j, 1) = 0;
            ima_out(i , j, 2) = 0;
            ima_out(i , j, 3) = 0;        
        end
    end
end
```



Le résultat est bien celui attendu :



### 2.2 Ferrari jaune

On peut remplacer le rouge par le jaune dans l'image en remplaçant la composante rouge des pixels dont l'intensité de rouge $n_r$ est supérieure à 0.7 par leur composante jaune :

```matlab
for i = 1:960
    for j = 1:1280
         nu = (ima_r(i,j))/(sqrt((ima_r(i,j))^2 + (ima_b(i,j))^2 + (ima_g(i,j))^2));
         # Si nu > 0.7 (rouge prépondérant), on remplace la composante rouge par la composante jaune 
           if nu > 0.7
              ima_out(i , j ,1) = ima(i , j, 1);
              ima_out(i , j ,2) = ima(i , j, 1);
              ima_out(i , j ,3) = ima(i , j, 3);
           else                        
              ima_out(i , j ,1) = ima(i , j, 1);
              ima_out(i , j ,2) = ima(i , j, 2);
              ima_out(i , j ,3) = ima(i , j, 3);          
        end
    end
end
```



Le résultat de l'exécution est bien celui attendu :

<img src="/home/pablo/seti/B5/TP1_image/Image/ferrari_out_GPU.jpg" alt="ferrari_out_GPU" style="zoom:50%;" />



### 2.3 Réduction du temps d'exécution sous Matlab

On peut optimiser dans un premier temps en calculant un tableau des niveaux de rouge des pixels en dehors des boucles :

```matlab
ima_r=ima(:,:,1);
ima_g=ima(:,:,2);
ima_b=ima(:,:,3);

nr=ima_r./sqrt(ima_r.^2+ima_g.^2+ima_b.^2);
for j=1:taille(2)
    for i=1:taille(1)
        if (nr(i,j)>0.7)
            ima_out(i,j,2)=ima(i,j,1);
        end
    end
end
```



Dans un second temps, on montre qu'on peut encore optimiser le code en utilisant un tableau `image_test` qui servira de masque logique pour le remplacement des composantes jaunes :  

 ```matlab
 ima_r=ima(:,:,1);
 ima_g=ima(:,:,2);
 ima_b=ima(:,:,3);
 
 nr=ima_r./sqrt(ima_r.^2+ima_g.^2+ima_b.^2);
 image_test=nr>0.7;
 
 image_tmp=ima_g;
 image_tmp(image_test)=ima_r(image_test);
 ima_out(:,:,2)=image_tmp();
 ```



On relève les temps d'exécution suivants pour les différentes version des codes Matlab : 

|           | Temps d'exécution (s) |
| --------- | --------------------- |
| Version 1 | 0.082                 |
| Version 2 | 0.029                 |
| Version 3 | 0.023                 |



Les optimisations sont donc valides et permettent d'accélérer plus de deux fois le temps de calcul par rapport à la version non optimisée.



## 3 Seuillage en C sous Visual Studio

Le code de la fonction de seuillage sur CPU est le suivant :

```c
void seuillage_C(float image_out[][SIZE_J][SIZE_I], float image_in[][SIZE_J][SIZE_I])
{
    for(int j = 0; j < SIZE_J; j++){
    	for(int i = 0; i < SIZE_I; i++){
            float n_r = image_in[0][j][i] / (image_in[0][j][i] * image_in[0][j][i] + image_in[1][j][i] * image_in[1][j][i] + 	image_in[2][j][i] * image_in[2][j][i]);
            
            if(n_r > 0.7)
                image_out[2][j][i] = image_in[1][j][i];
            else
                image_out[2][j][i] = image_in[2][j][i];

            image_out[1][j][i] = image_in[1][j][i];
            image_out[0][j][i] = image_in[0][j][i];
        }
    }
}
```

Le résultat de l'exécution est bien celui attendu :

<img src="/home/pablo/seti/B5/TP1_image/Image/ferrari_out_GPU.jpg" alt="ferrari_out_GPU" style="zoom:50%;" />

- Le temps d'exécution moyen sur CPU que j'ai relevé est de **23.95 ms**. Les performances de ce code C sont donc équivalentes à celle du code Matlab optimisé décrit dans la partie précédente. *Ce code C ayant été compilant sans optimisation du compilateur (option -O0 de `gcc`), on peut conjecturer que les performances d'un code optimisé seraient supérieures.* 

- Si on inverse l'ordre des boucles, on parcourt alors l'image dans le sens des lignes d'abord, puis des colonnes. Cet algorithme induit des erreurs de cache dues à une mauvaise utilisation de la localité spatiale en mémoire, ce qui ralentit l'exécution : on relève donc un temps d'exécution moyen de **37.62ms** pour cette version.

## 4 Seuillage en CUDA sous Visual Studio

Contenu de `seuillage_main.cu` :

```c++
__global__ void seuillage_kernel(float d_image_in[][SIZE_J][SIZE_I],float d_image_out[][SIZE_J][SIZE_I])
{
    // On récupère dans un premier temps la position globale (i,j) du thread dans la grille
    int i_first, j_first;
    int i, j;
    i_first = blockIdx.x*BLOCK_SIZE;
    j_first = blockIdx.y*BLOCK_SIZE;
    i=i_first +threadIdx.x;
    j=j_first +threadIdx.y;
    
    // On utilise i et j pour traiter un pixel de l'image
    float n_r = d_image_in[0][j][i] / sqrt((d_image_in[0][j][i] * d_image_in[0][j][i] + d_image_in[1][j][i] * d_image_in[1][j][i] + d_image_in[2][j][i] * d_image_in[2][j][i]));
    
    if(n_r > 0.7)
        d_image_out[2][j][i] = d_image_out[1][j][i];
    else
        d_image_out[2][j][i] = d_image_in[2][j][i];
    
    d_image_out[1][j][i] = d_image_in[1][j][i];
    d_image_out[0][j][i] = d_image_in[0][j][i];
}
```



Je remplis le code à trou de cette manière :

```c
// Alocation mémoire de d_image_in et d_image_out sur la carte GPU :
cudaMalloc((void**) &d_image_in, mem_size);
cudaMalloc((void**) &d_image_out, mem_size);

// On copie h_image_in depuis la mémoire hôte dans la mémoire device :
cudaMemcpy(d_image_in, h_image_in, mem_size, cudaMemcpyHostToDevice);

// On utilise des blocs de BLOCK_SIZE*BLOCK_SIZE threads
dim3 threads(BLOCK_SIZE, BLOCK_SIZE);
// Cette grille est composée de (SIZE_I/BLOCK_SIZEà * (SIZE_J/BLOCK_SIZE) blocs
dim3 grid(SIZE_I/BLOCK_SIZE, SIZE_J/BLOCK_SIZE);

// On lance les threads sur la carte GPU :  
seuillage_kernel<<<grid, threads>>>((float (*)[SIZE_J][SIZE_I])d_image_in, (float (*)[SIZE_J][SIZE_I])d_image_out);

// In copie d_image_out depuis la mémoire device sur la mémoire hôte : 
cudaMemcpy(h_image_out_GPU, d_image_out, mem_size, cudaMemcpyDeviceToHost);
```



L'image obtenue correspond bien à celle qui était attendue, et est bien la même que l'image obtenue à partir du code Matlab.

<img src="TP1_image\Image\ferrari_out_GPU.jpg" alt="ferrari_out_GPU" style="zoom:50%;" />

### 4.1 Puissances de calcul théoriques sur CPU et GPU

- La puissance de calcul du CPU des machines des salles TP est égale à $f_{cpu} * n_{cores} * IPC = 1.5GHz * 1 * 1 = 1.5 GFLOPS$.    

- La puissance de calcul du GPU des machines des salles TP est égale à $876MHz * 192 * 2 = 336.384 GFLOPS$ (un GPU effectuant un load-accumulate à chaque cycle).

  Théoriquement, les calculs pouvant être effectués sur GPU présentent donc un facteur d'accélération de 224. 

### 4.2 Temps d'exécution observés sur GPU 

Le temps d'exécution moyen obtenu sur une machine possédant **192 cores CUDA cadencés à (max) 876 MHz** est de **11.631648 ms** (avec un temps de transferts entre mémoires host et device de **78%** du temps d'exécution total). 

1. Le  facteur d'accélération est d'à peu près 2 entre la version CPU et la version GPU.

2. L'accélération obtenue sur une machine telle que la Tesla K40 (**2880 coeurs cadencés à 745Hz**) ne peut que concerner le temps d'exécution des threads, les temps de transfert mémoire étant indépendants du nombre de coeurs de la carte. Cette accélération est égale à $(2880 * 754) / (192 * 876) \approx 13$. Ainsi, on peut conjecturer à partir des résultats précédents que l'exécution totale sur cette machine durerait $(11*(1-0.78) / 12 + 11 * 0.78) \approx 8.6 ms$, soit un facteur d'accélération global de 1.27 entre les deux GPU.

     

# Rapport TP2 Réduction

### 1. Version naïve de l'algorithme de réduction sur GPU

On cherche ici à calculer la somme des éléments d'un tableau en utilisant une grille de threads. Pour cela, on procède de cette façon : 

1. On calcule un tableau d'entier `block_ranges`correspondant à une étendue d'élément à sommer pour chaque bloc de threads.

2. Pour diviser le travail à effectuer par chaque bloc entre ses threads, on décide qu'un thread d'un bloc devra sommer tous les éléments de `block_ranges` entre `range.x +threadIdx.x` et `range.y` avec un pas de `blockDim.x`.

   ```c++
    int my_sum = 0;
   for( int idx = range.x + threadIdx.x ; idx < range.y ; idx += blockDim.x )
       my_sum += in_buffer[idx];
   ```

3. On copie la somme du thread dans la mémoire shard :

   ```c++
   s_mem[threadIdx.x] = my_sum;
   ```

4. On place une barrière de synchronisation des threads afin de s'assurer que la mémoire shared contient bien toutes les sommes des threads : 

   ```c++
   __syncthreads();
   ```

5. Ensuite, la moitié inférieure des threads incrémente sa somme dans la mémoire shared avec la somme présente une moitié de threads plus loin en mémoire shared. Cette opération étant effectuée pour tous les threads, on synchronise avec une barrière afin d'avoir toutes les sommes présentes dans la mémoire shared et on recommence cette opération sur la moitié de l'ensemble de threads considéré précédemment, et ceci de suite jusqu'à ce que la somme totale du vecteur soit présente dans la première case de la mémoire shared.  

   **Schéma récapitulatif :** 

<img src="/home/pablo/Téléchargements/jjQvK.png" alt="jjQvK" style="zoom: 25%;" />

On code cette opération de la façon suivante : 

```c++
for(int i = 2; i <= blockDim.x; i*=2)
{
    if(threadIdx.x < blockDim.x / i )
    	s_mem[threadIdx.x] += s_mem[threadIdx.x + (blockDim.x / i)];
    __syncthreads();
}
```

**Note** 

On peut améliorer le temps d'exécution en sa passant de synchronisation sur le dernier warp, où tous les threads s'exécuteront en même temps :

```c++
int offset ;
for(offset = blockDim.x / 2 ; offset > 16 ; offset /= 2 )
{
    if( threadIdx.x < offset )
    	s_mem[threadIdx.x] += s_mem[threadIdx.x + offset];
    __syncthreads( );
}

for(; offset > 0 ; offset /= 2 )
{
    if( threadIdx.x < offset )
        s_mem[threadIdx.x] += s_mem[threadIdx.x + offset];
    //__syncthreads( );
}
```

 6. Chaque premier thread de bloc stocke le premier élément de la mémoire shared du bloc dans `out_buffer[blockIdx.x]`.

    ```c++
    if( threadIdx.x == 0 )
    	out_buffer[blockIdx.x] = s_mem[0];
    ```

Pour obtenir le résultat de la réduction avec cette fonction de réduction, on procède ensuite en deux temps : 

1. On applique cette fonction de réduction sur le vecteur à sommer en entier et on met le résultat dans un vecteur `partial_sums` qui contiendra alors les sommes partielles de chaque bloc. 
2. Ensuite, il suffit de réappliquer la fonction sur le vecteur `partial_sum` en stockant le résultat dans `partial_sums` pour obtenir la somme des sommes partielles dans le premier élément de `partial_sums`.

Le code de la fonction complète est détaillé en partie **3.**

### 2. Version optimisée en effectuant un calcul par Warps

On s'aperçoit qu'on pourrait optimiser la fonction de réduction décrite précédemment en faisant en sorte que les calculs des sommes partielles soient affectés à des warps de threads plutôt que des blocs de threads. Les warps sont les groupes de threads que la GPU exécute en même temps, sur chaque Streaming Multiprocessor, à chaque cycle. En effet, (texte traduit de https://developer.nvidia.com/blog/using-cuda-warp-level-primitives/) : 

*Les GPU NVIDIA exécutent des warps de 32 threads parallèles en SIMT, qui permet à chaque thread d'accéder à ses propres registres, de charger et de stocker à partir d'adresses divergentes et de suivre des chemins de flot de contrôle divergents. Le compilateur CUDA et le GPU travaillent ensemble pour s'assurer que les threads d'un warp exécutent ensemble les mêmes séquences d'instructions aussi souvent que possible afin de maximiser les performances.* 

Ainsi, dans la fonction de réduction par warp présentée ci-dessous, à l'étape "*Compute the sum inside each warp*", à chaque itération de la boucle for, on a la garantie que tous les résultats des sommes partielles sont présents dans la moitié inférieure de la mémoire shared après l'instruction `s_mem[threadIdx.x] += s_mem[threadIdx.x + (32 / i)];` : il n'y donc pas besoin de synchronisation entre threads à chaque itération, on peut se permettre de placer une barrière après la boucle for uniquement. 

Ensuite, comme chaque somme partielle est présente dans la première lane de chaque warp, le premier thread de la grille peut sommer tous les éléments des premières lane de cette façon (il n'y pas besoin de synchronisation dans la boucle car un seul thread est actif ici) :

```c++
if( warp_id == 0 && lane_id == 0 )
{
    // Read my value from shared memory and store it in a register.
    my_sum = s_mem[0];

    // Sum the results of the warps.
    for(int i = 1; i < NUM_WARPS; i++)
    {
    	s_mem[threadIdx.x] = my_sum += s_mem[threadIdx.x + 32 * i];
    }      
}
__syncthreads();
```

Ensuite, comme pour la fonction non optimisée, on procède ensuite deux temps pour calculer la somme totale : 

1. On applique cette fonction de réduction sur le vecteur à sommer en entier et on met le résultat dans un vecteur `partial_sums` qui contiendra alors les sommes partielles de chaque bloc. 
2. Ensuite, il suffit de réappliquer la fonction sur le vecteur `partial_sum` en stockant le résultat dans `partial_sums` pour obtenir la somme des sommes partielles dans le premier élément de `partial_sums`.

Le code de la fonction complète est détaillé en partie **3.**

On peut vérifier ensuite qu'on obtient les bon résultats pour les deux version de l'algorithme :

```
Computing on the CPU using 1 CPU thread
  Elapsed time: 457.878ms

Computing on the CPU using 4 OpenMP thread(s)
  Elapsed time: 122.548ms

Computing on the GPU using Thrust (transfers excluded)
  Elapsed time: 21.1403ms

Computing on the GPU (transfers excluded)
  Elapsed time: 53.1272ms

Computing on the GPU using a tuned version (transfers excluded)
  Elapsed time: 17.5379ms


OpenMP      results: ref= 67108864 / sum= 67108864
Thrust      results: ref= 67108864 / sum= 67108864
CUDA        results: ref= 67108864 / sum= 67108864
CUDA Optim  results: ref= 67108864 / sum= 67108864
```



### 3. Code des deux fonctions  de réduction

```c++
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// G P U   R E D U C T I O N

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void reduce_kernel( int n, const int *in_buffer, int *out_buffer, const int2 *block_ranges )
{
  // Allocate shared memory inside the block.
  extern __shared__ int s_mem[];

  // The range of data to work with.
  int2 range = block_ranges[blockIdx.x];

  // Compute the sum of my elements.
  int my_sum = 0;
  for( int idx = range.x + threadIdx.x ; idx < range.y ; idx += blockDim.x )
    my_sum += in_buffer[idx];

  // Copy my sum in shared memory.
  s_mem[threadIdx.x] = my_sum;

  // Make sure all the threads have copied their value in shared memory.
  __syncthreads();

  int offset ;
  // Compute the sum inside the block.
  for(offset = blockDim.x / 2 ; offset > 16 ; offset /= 2 )
  {
    if( threadIdx.x < offset )
      s_mem[threadIdx.x] += s_mem[threadIdx.x + offset];
    __syncthreads( );
  }

  //INSIDE WARP 0 SYNC NOT NECESSARY
  for(; offset > 0 ; offset /= 2 )
  {
    if( threadIdx.x < offset )
      s_mem[threadIdx.x] += s_mem[threadIdx.x + offset];
    //__syncthreads( );
  }


  // The first thread of the block stores its result.
  if( threadIdx.x == 0 )
    out_buffer[blockIdx.x] = s_mem[0];
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// G P U   R E D U C T I O N :   O P T I M I Z E D   V E R S I O N

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template< int BLOCK_DIM > 
__global__ void reduce_kernel_optimized( int n, const int *in_buffer, int *out_buffer, const int2 *__restrict block_ranges )
{
  // The number of warps in the block.
  const int NUM_WARPS = BLOCK_DIM / 32;
  float my_sum=0;

  // Allocate shared memory inside the block.
  __shared__ volatile int s_mem[BLOCK_DIM];

  // The range of data to work with.
  int2 range = block_ranges[blockIdx.x];

  // Warp/lane IDs.
  const int warp_id = threadIdx.x / 32;
  const int lane_id = threadIdx.x % 32;

  // Compute the sum of my elements.  
  for(int i = range.x; i < range.y; i += blockDim.x )
    my_sum += in_buffer[i + threadIdx.x];

  // Copy my sum in shared memory.
  s_mem[threadIdx.x] = my_sum;

  // Compute the sum inside each warp.
  for(int i = 2; i <= 32; i <<= 1)
  {
    if(lane_id < WARP_SIZE / i )
      s_mem[threadIdx.x] += s_mem[threadIdx.x + (32 / i)];
  }
  __syncthreads();
    
  if( warp_id == 0 && lane_id == 0 )
  {
    // Read my value from shared memory and store it in a register.
    my_sum = s_mem[0];

    // Sum the results of the warps.
    for(int i = 1; i < NUM_WARPS; i++)
    {
      s_mem[threadIdx.x] = my_sum += s_mem[threadIdx.x + 32 * i];
    }      
  }
  __syncthreads();

  // The 1st thread stores the result of the block.
  if( threadIdx.x == 0 )
    out_buffer[blockIdx.x] = my_sum;
}
```



