# TP1 : Analyse de performances

### Caractéristiques de ma machine

| Processeur                   | AMD Ryzen 2500U             |
| ---------------------------- | --------------------------- |
| Architecture                 | x86_64                      |
| Coeurs                       | 4                           |
| Taille du cache L1D          | 32K (8-way set associative) |
| Taille du cache L1I          | 64K                         |
| Taille du cache L2           | 512K                        |
| Taille du cache L3 (partagé) | 4096K                       |
| Politique d'écriture         | write-back                  |

## Optimisations du compilateur

On veut étudier l'évolution des performances de quelques fonctions de `tp1.c` pour différents niveaux d'optimisation. 

### Fonction `zero()`

J'ai choisi d'étudier en premier lieu la fonction de mise à zero des éléments d'un vecteur. J'ai fait varier la taille du vecteur et me suis intéressé à la *moyenne* du nombre de cycle par itération comme mesure des performances.

| <img src="/home/pablo/SETI/TP1/FIGURES/zero_float.png" alt="zero_2" style="zoom:67%;" /> |
| :----------------------------------------------------------: |
| *Performance de `zero()` pour différentes optimisations et type float* |

| <img src="/home/pablo/SETI/TP1/FIGURES/zero_double.png" alt="zero_2" style="zoom:67%;" /> |
| :----------------------------------------------------------: |
| *Performance de `zero()` pour différentes optimisations et type double* |

#### Interprétation des résultats : 

Les deux graphes montrent que l'écart de performance le plus flagrant concerne `O0` et `O1`, et que les performances entre `O1`, `O2` et `O3` sont sensiblement les mêmes quand la taille du vecteur est grande. `O3` est un peu avantagée pour les petits vecteurs. Je propose maintenant d'expliquer ces résultat.   

On commence par s'intéresser aux optimisations de `O1`. Pour cela, j'ai généré le code assembleur de `zero()` et je m'intéresse au code correspondant à la boucle intérieure de `zero()`. Les résultats sont pour N = 10 et type de donnée float.

```assembly
# Résultat avec compilation -O0
0000000000000b27 <zero>:
	...
     a26:	mov    -0x8(%rbp),%eax
     a29:	cltq   
     a2b:	lea    0x0(,%rax,8),%rdx
     a33:	lea    0x201e26(%rip),%rax        # 202860 <BF>
     a3a:	vxorpd %xmm0,%xmm0,%xmm0
     a3e:	vmovsd %xmm0,(%rdx,%rax,1)
     a43:	incl   -0x8(%rbp)
     a46:	cmpl   $0x63,-0x8(%rbp)
     a4a:	jle    a26 <zero+0x2e>
	...
     
# Résultat avec compilation -O1

0000000000000913 <zero>:
	...
     889:	movq   $0x0,(%rax)
     890:	add    $0x8,%rax
     894:	cmp    %rax,%rsi
     897:	jne    889 <zero+0x34>
	...
```

- La version`-O1` utilise le frame pointer `%rbp` pour calculer la borne de la boucle et l'indice d'itération, tandis que la version `-O1` utilise directement les registres généraux `%ax` et `%si`. Ceci permet de ne pas faire de calcul relativement au frame pointer: en effet, dans le corps de la boucle, `O0` charge la valeur de `%rbp` dans `%ax`, se sert de la valeur de `%ax` pour faire les calculs, incrémente le frame pointer, puis compare cette valeur à la borne de boucle alors que `O1` met simplement `M(%ax) ` à 0, incrémente `%ax` et compare cette valeur à la borne stockée précédemment dans `%si`.
- *La version `-O0` commence par tester la condition de sortie de boucle à la fin de son code. C'est l'instruction `a4a` qui nécessite un branchement pour être accédée. La version `-O1` réordonne des blocs de code pour supprimer ce premier branchement (transformation d'une structure while(...) en do...while).*
- La version `-01` inline les fonctions `start_timer()` et `stop_timer()` dans `zero()`. La perte de temps due au non-inlining  dans `O0` est notable pour les petites tailles de vecteur (le temps d'appel et retour des marqueurs n'étant pas négligeable par rapport au temps passé dans la boucle intérieure).

Ces trois différences correspondent aux passes  `-freorder-blocks` et `-fomit-frame-pointer`  de gcc, présentes dans `-O1` et pas dans `-O0`. 

Maintenant, nous allons nous intéresser aux écarts de performances entre  `O1`, `O2` et `O3`.

```assembly
# Résultat avec compilation -O2

00000000000008a0 <zero>:
	...
     8e0:	movq   $0x0,(%rax)
     8e7:	add    $0x8,%rax
     8eb:	cmp    %rax,%rsi
     8ee:	jne    8e0 <zero+0x40>
	...
```

- Le code de la boucle intérieure est le même pour `O2` que pour `O1`. Pour cette fonction, les différences de performance entre ces deux optimisations ne sont pas significatives.
- Pour la compilation `-O3`, `-floop-unroll-and-jam ` permet de dérouler la boucle sur plusieurs itérations. `-fvect-cost-model=dynamic ` permet de vectoriser les instructions qui pourraient potentiellement accélerer l'exécution du code.  Je n'analyserai pas le code produit par gcc avec cette option car il est trop compliqué, mais je noterai la présence d'instructions vectorielles ainsi que d'un déroulage complet de la boucle intérieure. 
- Le nombre moyen de cycle par itération converge pour `O1`, `O2` et `O3` pour des grandes tailles de vecteurs les capacity cache miss masquent le gain en performance de `O3`... bref.



### Fonction `zero()` 









## Partie 3

- Si une ligne de mon cache peut contenir `k` floats, alors on a un compulsory write-miss toutes les `k` exécutions de la boucle intérieure. Ces write-miss  induisent plusieurs cycles de latences qui correspondent au chargement de la ligne depuis la mémoire centrale dans le cache L1 (mon cache est write-back). Pour les petites tailles de vecteurs, cette pénalité représente une proportion important du temps d'exécution de la boucle intérieure de `zero()`. Ceci explique pourquoi on observe un plus grand nombre moyen de cycles par itération pour des petites tailles de vecteur que pour des moyennes tailles de vecteur.
- Mon cache L1D faisant 32K, on devrait voir les premiers capacity miss pour une taille de vecteur égale à $2^{15} / 2^2 = 2^{13} = 8192$ float, ou $2^{15} / 2^3 = 2^{12} = 4096$ double. 
