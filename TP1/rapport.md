# TP1 : Analyse de performances

### Caractéristiques de ma machine

| Processeur                   | AMD Ryzen 2500U |
| ---------------------------- | --------------- |
| Architecture                 | x86_64          |
| Coeurs                       | 4               |
| Taille du cache L1D          | 32K             |
| Taille du cache L1I          | 64K             |
| Taille du cache L2           | 512K            |
| Taille du cache L3 (partagé) | 4096K           |
| Politique d'écriture         | write-back      |

## Optimisations du compilateur

On veut étudier l'évolution des performances de quelques fonctions de `tp1.c` pour différents niveaux d'optimisation. 

### Fonction `zero()`

J'ai choisi d'étudier en premier lieu la fonction de mise à zero des éléments d'un vecteur. 



|        ![zero_2](/home/pablo/SETI/FIGURES/zero_2.png)        |
| :----------------------------------------------------------: |
| *Performance de `zero()` pour différentes optimisations et type int* |

