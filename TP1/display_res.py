import subprocess
import os
import matplotlib.pyplot as plt
import numpy as np
from numpy.random import randn

N = [10, 100, 500, 1000, 1500, 2000]
# OPT = ['-O1', '-O2', '-O3']
OPT = ['-O0', '-O1', '-O2', '-O3']
PERF = np.zeros((len(OPT), len(N)), dtype=float)
options = ""
# options = ""


def plot_perf():
    fig = plt.figure()
    ax = fig.add_axes([0.1, 0.1, 0.6, 0.75])

    for i in range(len(OPT)):
        plt.plot(N, PERF[i, :], marker='x', label=str(OPT[i]))
        plt.xticks(N)
        plt.xlabel("Taille du vecteur")
        plt.ylabel("Nombre moyen de cycles par itération")
        plt.legend(bbox_to_anchor=(1.05, 1),
                   loc='upper left', borderaxespad=0.)
    plt.show()


for i in range(len(OPT)):
    for j in range(len(N)):
        cmd = "bash -c "+"'"+"gcc " + \
            OPT[i] + " -DN="+str(N[j]) + \
            options + \
            " -DTYPE=double -DPYTHON -march=native  tp1.c -o tp1; ./tp1'"
        print(cmd)
        res = float(subprocess.getoutput(cmd))
        PERF[i, j] = res
        os.system("bash -c 'objdump -d --no-show-raw-insn tp1 > asm/tp1" +
                  OPT[i] + "-N" + str(N[j]) + ".lss'")

plot_perf()
