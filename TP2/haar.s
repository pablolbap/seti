	;; MIPS program double ax plus y (daxpy)
      .data
A:    .double 8.0, 1.0,2.0,3.0,4.0,5.0,7.0,8.0,9.0,10.0,11.0,12.0
B:    .double 3.0,14.0,13.0,4.0,8.0,9.0,8.0,7.0,6.0,5.0,4.0,3.0
N:    .word 12


              .text
main:
	ld r1,N(r0)		; r1=N
	daddi r3,r0,A		; r3=@A
	daddi r4,r0,B		; r4=@B
loop:	l.d f1,0(r3)		; f1=A[i]
	l.d f2,0(r4)		; f2=B[i]
        ;; on teste si l'iteration est paire ou impaire
	andi r5,r1,0x1          ; r5 == parité de i
        beq  r5,r0,pair         ; branchement si LSB de i==0
	;; cas impair
	add.d f2,f2,f1		; Bi=Bi+Ai
        beq  r0,r0,fin          ; éviter l'instruction suivante
        ;;;  cas pair
pair:   
	sub.d f2,f2,f1		; Bi=Bi-Ai
fin:    
        ;;  increment pendant la latence de l'add/sub FP
	daddi r1,r1,-1		; N--
        daddi r3,r3,8           ; @A++
        daddi r4,r4,8           ; @B++
	s.d   f2,-8(r4)		; ranger B[i]
	bne   r2,r0,loop		; 
	nop
	halt          

        ;;  version optimisée avec déroulage de boucle d'ordre 2
        
              .text
main:
	ld r1,N(r0)		; r1=N
	daddi r3,r0,A		; r3=@A
	daddi r4,r0,B		; r4=@B
loop:	l.d f0,0(r3)		; f0=A[i]
	l.d f2,0(r4)		; f2=B[i]
	l.d f4,8(r3)		; f4=A[i+1]
	l.d f6,8(r4)		; f6=B[i+1]
	sub.d f2,f2,f0		; Bi=Bi-Ai
	add.d f4,f4,f6		; Bi=Bi+Ai
	daddi r1,r1,-2		; N-- 
        daddi r3,r3,16          ; @A++
        daddi r4,r4,16          ; @B++
	s.d   f2,-16(r4)	; ranger B[i]
	s.d   f4,-8(r4)	        ; ranger B[i+1]
	bne   r2,r0,loop		; 
	nop
	halt          
