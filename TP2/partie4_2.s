.data
    N:  .word   12
    A:  
    somme: .double 0.0 

.text
    main:
    daddi r3, r0, A; r3=&A
    ld r2,N(r0)		; r2=N
loop:	
    l.d f1,0(r3)		; f1=A[i]
    l.d f2,somme(r0)	; f2=somme
    add.d f2,f2,f1		; f2=somme +A[i]
    s.d f2,somme(r0)    ; somme=f2
    daddi r3,r3,8		; i = i + 1
    daddi r2,r2,-1		; N = N - 1
    bne r2,r0,loop		; 
    nop
    halt 