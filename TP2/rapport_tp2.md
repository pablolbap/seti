# TP2 : Simulateur de processeur MIPS

## Etude du pipeline sur des programmes élémentaires

J'obtiens les résultats suivants pour les simulations d'exécution des différentes fonctions.

| Programme | # Cycles | CPI   | # Cycles (fwd) | CPI (fwd) |
| --------- | -------- | ----- | -------------- | --------- |
| `sum.s`   | 13       | 2.6   | 10             | 2.0       |
| `mul.s`   | 16       | 2.666 | 11             | 1.833     |
| `mulf.s`  | 19       | 3.8   | 16             | 3.2       |
| `sumf.s`  | 16       | 3.2   | 13             | 2.6       |
| `divf.s`  | 36       | 7.2   | 33             | 6.6       |
| `abs.s`   | 19       | 2.375 | 16             | 2.0       |

J'obtiens les résultats suivants pour les latences des différents opérateur, avec et sans forwarding.

| Programme | Latence | Latence (fwd) |
| --------- | ------- | ------------- |
| `dadd`    | 3       | 1             |
| `dmult`   | 3       | 1             |
| `add.d`   | 6       | 4             |
| mul.d     | 9       | 7             |
| `div.d`   | 26      | 24            |

Exécution de double : la multiplication ne se comporte pas comme l'addition. En effet, l'opérateur de division est non pipeliné, ce qui oblige le processeur à staller la deuxième division en attendant que la première se finisse et libère le bloc de division (aléa structurel). Ceci est indiqué par le label "StDiv" par le simulateur.



![Capture d’écran de 2021-11-08 09-40-35](/home/pablo/Images/Capture d’écran de 2021-11-08 09-40-35.png)

Une solution pour optimiser ce programme serait d'utiliser de faire C2 = B1 * (1 / B2)  C1 = A1 * (1/A2) + pipeline logiciel ?

```assembly
# Note : équivalent

ld r7, A(r0) 
<=>
addi r11, r7, 0
ld r10, 0(r11)

# Plus souple
```

## Comportement de programmes complexes 

| Programme | # Cycles | CPI   | # Cycles (fwd) | CPI (fwd) |
| --------- | -------- | ----- | -------------- | --------- |
| `fib.s`   | 153      | 1.961 | 101            | 1.294     |
| `daxpy.s` | 309      | 3.059 | 224            | 2.217     |

| Programme | # Suspension | # Suspension (fwd) |
| --------- | ------------ | ------------------ |
| `fib.s`   | 62           | 10                 |
| `daxpy.s` | 193          | 108                |



Optimisation éviter d'aller en mémoire pour chercher des choses qui sont déjà dans des registres

Quand on sort de la boucle il faut aller avoir un état des registres qui permet à l'itération suivante de s'exécuter



### Optimisation de `fib.s`

```assembly
; Compute first twelve Fibonacci numbers and put in array, then print
      .data
fibs: .space 48          ; "array" of 12 32_bits words to contain fib values
size: .word  12          ; size of "array" 
      .text
      daddi $t0, r0, fibs    ; load address of array
      lw   $t5, size(r0)    ; load array size
      daddi $t3, r0,1        ; 1 is first and second Fib. number
      daddi $t4, r0,1        ; 1 is first and second Fib. number
      daddi $t1, $t5, -2     ; Counter for loop, will execute (size-2) times
      sw   $t3, 0($t0)      ; F[n+1] = F[n]
      sw   $t4, 4($t0)      ; F[n+2] = F[n+1]
loop: 
	  add  $t2, $t3, $t4    ; $t2 = F[n] + F[n+1] = F[n+2]
      add $t3, $t4, r0      ; $t3 = $t4
      add $t4, $t2, r0      ; $t4 = $t2
      sw   $t4, 8($t0)      ; Store F[n+2] = F[n] + F[n+1] in array
      daddi $t0, $t0, 4      ; increment address of Fib. number source
      daddi $t1, $t1, -1     ; decrement loop counter
      bnez $t1, loop        ; repeat if not finished yet.
      nop
      halt
```

On supprime les accès mémoire et on passe à **92 cycles, 1.164 CPI et 0 suspensions.**

### Optimisation de `daxpy.s` 

```assembly
main:
			daddi r3, r0, X; r3=&X
			daddi r4, r0, Y; r4=&Y
			l.d f0,A(r0)		; f0 = A
			ld r2,N(r0)		; r2=N
loop:	
			l.d f1,0(r3)		; f1=x[i]
			l.d f3,8(r3)		; f3=x[i+1]
			l.d f2,0(r4)		; f2=y[i]
			l.d f4,8(r4)		; f4=y[i+1]
			mul.d f1,f1,f0		; f1=axi
			add.d f2,f2,f1		; f2=axi+yi
			mul.d f3,f3,f0		; f3=ax[i+1]
			add.d f4,f4,f3		; f4=ax[i+1]+y[i+1]
			s.d f2,0(r4)		; y[i]=f2
			s.d f4,8(r4)		; y[i+1]=f4
			daddi	r3,r3,16		; incremeting index for both x and y
			daddi	r4,r4,16		; 
			daddi r2,r2,-2		; N = N - 2
			bne r2,r0,loop		; 
			nop
			halt    
```

On effectue un déroulage de boucle d'ordre 2 et on passe à **183 cycles, 2.033 CPI et 84 suspensions.**

## Ecriture de programme assembleur

#### <u>Premier programme</u>

Programme naïf avec jump.

```assembly
    .data
N:  .word   12
A:  
B: 

    .text
main:
    daddi r3, r0, A; r3=&A
    daddi r4, r0, B; r4=&B
    dadd r1,r0,r0		; r1=i=0
    ld r2,N(r0)		; r2=N
    daddi r5, r0, 0; r5=0
loop:	
    l.d f1,0(r3)		; f1=A[i]
    l.d f2,0(r4)		; f2=B[i]
    bne r5, r0, impair  ; if 5 != 0 jump to even
    nop
even:			
    add.d f2,f2,f1		; f2=B[i]+A[i]
    daddi r5, r5, 1;
    j always;			
uneven:
    sub.d f2,f2,f1		; f2=B[i]-A[i]
    daddi r5, r5, -1;
always:
    s.d f2,0(r4)		  ; B[i]=f2
    daddi	r3,r3,8		; Incrementing index for A and B
    daddi	r4,r4,8		; 
    daddi r2,r2,-1		; N = N - 1
    bne r2,r0,loop		; 
    nop
    halt    
```

Tel quel, le programme entraîne **12 suspensions (donc 1 par itération de la boucle).**

```assembly
.data
    N:  .word   12
    A:  
    B: 

.text
    main:
    daddi r3, r0, A; r3=&A
    daddi r4, r0, B; r4=&B
    dadd r1,r0,r0		; r1=i=0
    ld r2,N(r0)		; r2=N
    daddi r5, r0, 0; r5=0
loop:	
    l.d f1,0(r3)		; f1=A[i]
    l.d f2,0(r4)		; f2=B[i]
    l.d f3,8(r3)		; f1=A[i+1]
    l.d f4,8(r4)		; f1=B[i+1]
    add.d f2,f2,f1		; f2=B[i]+A[i]
    sub.d f4,f4,f3		; f4=B[i+1]-A[i+1]
    s.d f2,0(r4)		  ; B[i]=f2
    s.d f4,8(r4)		  ; B[i+1]=f4
    daddi	r3,r3,16		; Incrementing index for A and B
    daddi	r4,r4,16		; 
    daddi r2,r2,-2		; N = N - 2
    bne r2,r0,loop		; 
    nop
    halt  
```

En regroupant les itérations paires et impaires, on peut supprimer les aléas de branchements et le test d'un registre pour pair ou impair. Cela améliore le  nombre de cycles mais il y a encore des dépendances RAW entre l'addition et la soustraction et l'écriture de B. En reordonnant de cette manière 

```assembly
.data
    N:  .word   12
    A:  
    B: 

.text
    main:
    daddi r3, r0, A; r3=&A
    daddi r4, r0, B; r4=&B
    ld r2,N(r0)		; r2=N
loop:	
    l.d f1,0(r3)		; f1=A[i]
    l.d f2,0(r4)		; f2=B[i]
    l.d f3,8(r3)		; f1=A[i+1]
    l.d f4,8(r4)		; f1=B[i+1]
    add.d f2,f2,f1		; f2=B[i]+A[i]
    sub.d f4,f4,f3		; f4=B[i+1]-A[i+1]
    daddi	r3,r3,16		; Incrementing index for A and B
    daddi	r4,r4,16		; 
    s.d f2,-16(r4)		  ; B[i]=f2
    s.d f4, -8(r4)		  ; B[i+1]=f4
    daddi r2,r2,-2		; N = N - 2
    bne r2,r0,loop		; 
    nop
    halt 
```

on peut supprimer les dépendances RAW. Le programme est fortement optimisé.



#### <u>Deuxième programme</u>



```assembly
.data
    N:  .word   12
    A:  
    somme: .double 0.0 

.text
    main:
    daddi r3, r0, A; r3=&A
    ld r2,N(r0)		; r2=N
loop:	
    l.d f1,0(r3)		; f1=A[i]
    l.d f2,somme(r0)	; f2=somme
    add.d f2,f2,f1		; f2=somme +A[i]
    s.d f2,somme(r0)    ; somme=f2
    daddi r3,r3,8		; i = i + 1
    daddi r2,r2,-1		; N = N - 1
    bne r2,r0,loop		; 
    nop
    halt 
```

##### On a **48 RAW** dépendances

(4 par itération de la boucle)